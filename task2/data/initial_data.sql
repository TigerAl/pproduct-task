insert into conversion_ratio (unit1, unit2, ratio) values ('min', 'sec', 60);
insert into conversion_ratio (unit1, unit2, ratio) values ('km', 'mile', 0.621371192);
insert into conversion_ratio (unit1, unit2, ratio) values ('km', 'm', 1000);
insert into conversion_ratio (unit1, unit2, ratio) values ('km', 'mm', 1000000);
insert into conversion_ratio (unit1, unit2, ratio) values ('sm', 'mm', 10);
insert into conversion_ratio (unit1, unit2, ratio) values ('sm', 'in', 0.393700787);
