/**
 * Describes the conversion ratio between two measurement units.
 */
public class ConversionRatio {
    private String unit1;
    private String unit2;
    private double ratio;
}
