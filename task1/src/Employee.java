/**
 * Plain Java object which represents the Employee entity, which was mentioned in task description.
 */
public class Employee {

    private int id;
    private String name;
    private int age;
    private double salary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object employee) {
        if (!(employee instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) employee;
        return (
                this.getId() == other.getId()
             && (   (this.getName() == null && other.getName() == null)
                 || (this.getName() != null && this.getName().equals(other.getName())) )
             && this.getAge() == other.getAge()
             && this.getSalary() == other.getSalary()
        );
    }
}
