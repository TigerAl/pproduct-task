create table conversion_ratio (
	unit1 VARCHAR(30) NOT NULL,
  unit2 VARCHAR(30) NOT NULL,
  ratio DOUBLE NOT NULL,
  PRIMARY KEY (unit1, unit2)
);

