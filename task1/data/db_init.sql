create table employee (
	id INT PRIMARY KEY,
	name VARCHAR_IGNORECASE(50),
	age INT,
	salary DECIMAL(10,2)
);