import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import sun.misc.IOUtils;

/**
 * Create the list of employees in DB and test perform sorting task.
 *
 * Mocked data was generated with an http://www.mockaroo.com online tool.
 */
public class Main {

    /**
     * Connection isn't closed in the finally section because this is a main application method and connection will be
     * closed automatically on exit.
     */
    public static void main(String[] args) throws SQLException, IOException {
        Connection connection  = DriverManager.getConnection("jdbc:hsqldb:mem:.", "sa", "sa");
        Statement statement = connection.createStatement();

        // init DB structure
        executeSQLFromResource(statement, Main.class.getResourceAsStream("/db_init.sql"));
        // insert mocked data
        executeSQLFromResource(statement, Main.class.getResourceAsStream("/mocked_data.sql"));

        // read list of Employees from DB
        String sql = "SELECT * FROM employee";
        ResultSet resultSet = statement.executeQuery(sql);
        List<Employee> employeeList = collectEmployeeList(resultSet);

        String outputLineTemplate = "id = %d, name = %s, age = %d, salary = $%.2f";
        for (Employee e : employeeList) {
            System.out.println(String.format(outputLineTemplate, e.getId(), e.getName(), e.getAge(), e.getSalary()));
        }

        //perform SQL based sorting

        //remember current system time in order to compare algorithm efficiency.
        long startTimestamp = System.currentTimeMillis();
        sql = "SELECT * FROM employee ORDER BY age ASC, name ASC";
        resultSet = statement.executeQuery(sql);
        List<Employee> sortedEmployeeList = collectEmployeeList(resultSet);
        long duration1 = System.currentTimeMillis() - startTimestamp;

        for (Employee e : sortedEmployeeList) {
            System.out.println(String.format(outputLineTemplate, e.getId(), e.getName(), e.getAge(), e.getSalary()));
        }

        //perform SQL based sorting
        startTimestamp = System.currentTimeMillis();
        Comparator<Employee> comparator = new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                int ageComparisonResult = o1.getAge() - o2.getAge();
                if (ageComparisonResult == 0) {
                    //lets made an assumption that o1.getName() and o2.getName() never return null value.
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
                return ageComparisonResult;
            }
        };
        Employee[] employeeArray = employeeList.toArray(new Employee[employeeList.size()]);
        Arrays.sort(employeeArray, comparator);
        List<Employee> sortedEmployeeList2 = Arrays.asList(employeeArray);
        long duration2 = System.currentTimeMillis() - startTimestamp;

        for (Employee e : sortedEmployeeList2) {
            System.out.println(String.format(outputLineTemplate, e.getId(), e.getName(), e.getAge(), e.getSalary()));
        }

        //print performance comparison
        System.out.println("--- Sort methods comparison results ---");
        System.out.println(String.format("SQL based sorting duration = %.3f seconds", duration1 / 1000.0));
        System.out.println(String.format("Java Arrays based sorting duration = %.3f seconds", duration2 / 1000.0));

        //compare that both lists are equals
        boolean isEquals = true;
        for (int i = 0; i < sortedEmployeeList.size(); i++) {
            if (!sortedEmployeeList.get(i).equals(sortedEmployeeList2.get(i))) {
                isEquals = false;
                break;
            }
        }
        System.out.println(String.format("Sorted lists are " + (isEquals ? "equals." : " not equals.")));
    }

    /**
     * This method read text data from specified file and use it as SQL query to execute on DB side.
     *
     * @param statement - used to communicate with SQL DB server and execute SQL query which was recovered from file.
     * @param resourceStream - input stream which refers to the text file in ANSI encoding which contains SQL query.
     * @throws IOException in a case of file open/read errors.
     * @throws SQLException if it was raised from statement.execute(...) method call.
     */
    private static void executeSQLFromResource(Statement statement, InputStream resourceStream)
            throws IOException, SQLException {
        byte[] data = IOUtils.readFully(resourceStream, -1, true);
        resourceStream.close();
        String sql = new String(data);
        statement.execute(sql);
    }

    /**
     * Collect Employee objects data from the resultSet and stack it into java List collection.
     *
     * @param resultSet SQL resultSet which contains data from employee data base table. Each row represents one
     *                  Employee object.
     * @return List of newly created Employee objects.
     * @throws SQLException if something wrong with a resultSet.
     */
    private static List<Employee> collectEmployeeList(ResultSet resultSet) throws SQLException {
        List<Employee> employeeList = new ArrayList<Employee>();
        while (resultSet.next()) {
            Employee employee = new Employee();
            employee.setId(resultSet.getInt("id"));
            employee.setName(resultSet.getString("name"));
            employee.setAge(resultSet.getInt("age"));
            employee.setSalary(resultSet.getDouble("salary"));
            employeeList.add(employee);
        }
        return employeeList;
    }
}
