import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import sun.misc.IOUtils;

/**
 * Create the list of employees in DB and test perform sorting task.
 *
 * Mocked data was generated with an http://www.mockaroo.com online tool.
 */
public class Main {

    /**
     * Connection isn't closed in the finally section because this is a main application method and connection will be
     * closed automatically on exit.
     */
    public static void main(String[] args) throws SQLException, IOException {
        Connection connection  = DriverManager.getConnection("jdbc:hsqldb:file:db;close_result=true", "sa", "sa");
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'conversion_ratio'";
        ResultSet resultSet = statement.executeQuery(sql);

        // init DB structure and insert mocked data if initial data structure isn't initialised yet.
        if (resultSet.next()) {
            executeSQLFromResource(connection, Main.class.getResourceAsStream("/db_init.sql"));
            executeSQLFromResource(connection, Main.class.getResourceAsStream("/initial_data.sql"));
        }

        List<String> list = getAllUnits(connection);
    }

    /**
     * This method read text data from specified resourceStream and use it as SQL query to execute on DB side.
     *
     * @param connection - used to communicate with SQL DB and execute SQL query which is stored in resource stream.
     * @param resourceStream - input stream which refers to the text file in ANSI encoding which contains SQL query.
     * @throws IOException in a case of file open/read errors.
     * @throws SQLException if it was raised from statement.execute(...) method call.
     */
    private static void executeSQLFromResource(Connection connection, InputStream resourceStream)
            throws IOException, SQLException {
        byte[] data = IOUtils.readFully(resourceStream, -1, true);
        resourceStream.close();
        String sql = new String(data);
        Statement statement = connection.createStatement();
        statement.execute(sql);
        statement.close();
    }

    /**
     * Will return all measurement units which was mentioned in DB.
     *
     * @param connection connection to DB.
     * @return List of measurement units names.
     * @throws SQLException could be thrown if something wrong happened with a DataBase.
     */
    private static List<String> getAllUnits(Connection connection) throws SQLException {
        String sql = "SELECT unit1 as unit from conversionRatio UNION " +
                     "SELECT unit2 as unit from conversionRatio";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<String> result = new ArrayList<String>();
        while (resultSet.next()) {
            result.add(resultSet.getString("unit"));
        }
        statement.close();
        return result;
    }

    /**
     * Will return all measurement units which occurs in a pairs with the baseUnit.
     *
     * @param connection connection to DB.
     * @param baseUnit the base unit name which occurrences we are looking both in unit1 and unit2 columns.
     * @return List of measurement units names.
     * @throws SQLException could be thrown if something wrong happened with a DataBase.
     */
    private static List<String> getAllRelatedUnits(Connection connection, String baseUnit) throws SQLException {
        String sql = "SELECT unit2 as unit from conversionRatio where unit1 = '" + baseUnit + "'" + " UNION " +
                     "SELECT unit1 as unit from conversionRatio where unit2 = '" + baseUnit + "'";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<String> result = new ArrayList<String>();
        while (resultSet.next()) {
            result.add(resultSet.getString("unit"));
        }
        statement.close();
        return result;
    }

    /**
     * Will return the conversion ratio between two specified measurement units.
     *
     * @param connection connection to DB.
     * @param baseUnit the base unit name which occurrences we are looking both in unit1 and unit2 columns.
     * @param targetUnit the base unit name which occurrences we are looking both in unit1 and unit2 columns.
     * @return the conversion ratio between two specified measurement units
     * @throws SQLException could be thrown if something wrong happened with a DataBase.
     */
    private static double getConversionRatio(Connection connection, String baseUnit, String targetUnit)
            throws SQLException {
        String sql = "SELECT ratio from conversionRatio where unit1 = '"+ baseUnit +"' AND unit2 = '"+ targetUnit +"' "
                + " UNION "
                + "SELECT ratio from conversionRatio where unit2 = '"+ baseUnit +"' AND unit1 = '"+ targetUnit +"' ";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<String> result = new ArrayList<String>();

        while (resultSet.next()) {
            statement.close();
            return resultSet.getDouble("ratio");
        }
        statement.close();
        return -1;
    }
}
