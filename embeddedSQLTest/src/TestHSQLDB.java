import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Create the list of employees in DB and test perform sorting task.
 *
 * Mocked data was generated with an http://www.mockaroo.com online tool.
 */
public class TestHSQLDB {

    public static void main(String[] args) throws SQLException, IOException {
        Connection connection  = DriverManager.getConnection("jdbc:hsqldb:mem:.", "sa", "sa");
        Statement statement = connection.createStatement();

        // init DB structure
        executeSQLFromFile(statement, new File("./data/db_init.sql"));
        // insert mocked data
        executeSQLFromFile(statement, new File("./data/mocked_data.sql"));

        // read list of Employees from DB
        String sql = "SELECT * FROM employee";
        ResultSet resultSet = statement.executeQuery(sql);
        String outputLineTemplate = "id = %d, name = %s, age = %d, salary = $%.2f";
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int age = resultSet.getInt("age");
            double salary  = resultSet.getDouble("salary");
            System.out.println(String.format(outputLineTemplate, id, name, age, salary));
        }
    }

    /**
     * This method read text data from specified file and use it as SQL query to execute on DB side.
     *
     * @param statement - used to communicate with SQL DB server and execute SQL query which was recovered from file.
     * @param file - text file in ANSI encoding which contains SQL query.
     * @throws IOException in a case of file open/read errors.
     * @throws SQLException if it was raised from statement.execute(...) method call.
     */
    private static void executeSQLFromFile(Statement statement, File file) throws IOException, SQLException {
        Reader fileReader = new FileReader(file);
        // because the target file is in ANSI/UTF-8 encoding and it contain only ANSI chars, then we could use the file
        // length as size value of the read buffer. Regardless of the BOM symbol the read buffer size would be enough.
        char[] readBuffer = new char[(int) file.length()];
        fileReader.read(readBuffer);
        String sql = new String(readBuffer);
        statement.execute(sql);
    }
}
