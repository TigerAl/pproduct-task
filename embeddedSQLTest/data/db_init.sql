create table employee (
	id INT PRIMARY KEY,
	name VARCHAR(50),
	age INT,
	salary DECIMAL(10,2)
);